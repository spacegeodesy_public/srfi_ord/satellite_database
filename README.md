# Satellite Database

# About the Database

The [Satellite Database](https://satdb.ethz.ch/) is an Open Research project by the Space Geodesy Group at ETH Zurich. It archives orbital data of satellites and other space objects in the form of Two-Line Elements.



## Database Schema


![Database Schema](static/images/satdb_diagram.jpg)





# About the Source Code

The application was developed using Django. The functionalities are split into three applications: database_management, website and api. As a brief overview, these are the most important files and functionalities of each app:

## database_management

Database_management handles the database and the reading in of new data.

* models.py: All the models for the database. The database schema in the previous section gives an overview of what they contain and how they relate to each other.

* tle_utils.py: Various utility functions relating to processing the TLE files, most importantly the scrape_tle_data function and TLEObject class. TLEObject is initialized using a single TLE and automatically extracts, assigns and validates various values from the TLE. scrape_tle_data is used for scraping the .tle files from the CelesTrak website.

* management/commands: Custom commands for managing the database, most importantly scrape.py, which scrapes & reads in new data from CelesTrak.

## website

Website contains all the views and templates for the website. It also handles everything regarding search queries that are entered through the website.

* templates & static: All html templates and static files (images, css) for the website.

* views.py: Views for the website and various functions used for validating queries and fetching data. Includes the views for the webpages (homepage, api_documentation, about), querying and displaying the search results (search_results) and the download button (search_results_download). Important helper functions defined here are clean_query, which validates and cleans the query from the homepage(using frequency_list_formatter & datetime_formatter) and fetch_data, the function which handles the construction of the queryset from a validated query.


## api

Api handles the way the API retrieves and displays query results.

* Developed using [Django REST Framework](https://www.django-rest-framework.org/).

* views.py: Viewsets unique to the API. SatelliteDataViewset handles the retrieval and validation of the API search parameters (using helper functions like the datetime_formatter and api_frequency_list_formatter). Uses fetch_data from website/views.py for retrieving the queryset.



## Pointers for Changing the Code

These are a few pointers for people who might want/need to change this code in the future.


* The query parameters that are displayed on the search_results page (after entering them on the homepage) are already validated. This means that if there is a discrepancy between the values entered in the form and what parameters are displayed on the results page, the problem likely lies in the clean_query function (website/views.py).

* If you need to change how the search works (e.g. make the name search case sensitive), the queryset is constructed in fetch_data (website/views.py). This is also the only (completely) shared function between the homepage search and API, so if something affects both of them, it is a likely source of error.

* If you want the API to return more fields (other than id, norad_str and satellite), you can add new fields to the SatelliteDataSerializer in api/serializers.py.

* For the API, the number of TLEs returned per page is defined in Satellite_Database/settings.py under REST_FRAMEWORK = { ..., 'PAGE_SIZE'=500}.

* The admin page is configured in database_management/admin.py. If you want the admin page to e.g. display more columns for a model, you can configure that here.

* Frequency Sources are assigned to Satellites using a post_save signal. The receiver for this signal is update_satellite_frequency_source in database_managmenet/models.py.

* The regular updates to the database are done using a cronjob which runs the custom scrape command. This command is defined in database_management/management/commands/scrape.py. This code is responsible for creating all new SatelliteData and Satellite entries. It mainly utilizes the TLEObject class from database_management/tle_utils.py.


## Requirements

```
asgiref==3.6.0
astropy==5.2.1
beautifulsoup4==4.11.2
bs4==0.0.1
certifi==2022.12.7
charset-normalizer==3.0.1
Django==4.1.6
django-debug-toolbar==4.1.0
django-environ==0.9.0
djangorestframework==3.14.0
idna==3.4
mysqlclient==2.1.1
numpy==1.24.2
packaging==23.0
psycopg2-binary==2.9.6
pyerfa==2.0.0.1
pytz==2022.7.1
PyYAML==6.0
requests==2.28.2
soupsieve==2.3.2.post1
sqlparse==0.4.3
tzdata==2022.7
urllib3==1.26.14
```
