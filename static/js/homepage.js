

function expandbutton(x) {
    var x = document.getElementById(x);
    if (x.style.display === "none") {
        x.style.display = "block";
    } else {
        x.style.display = "none";
    }
}

var expandbuttons = document.getElementsByClassName("more-info")

for (var i=0; i < expandbuttons.length; i++) {
    expandbutton(expandbuttons[i]);
}
