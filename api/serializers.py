from rest_framework import serializers
from database_management.models import SatelliteData, Satellite





class SatelliteSerializer(serializers.ModelSerializer):
    """ Serializer for Satellites """

    class Meta:
        model = Satellite
        fields = ('__all__')




class SatelliteDataSerializer(serializers.Serializer):
    """Serializer for SatelliteData """

    id = serializers.IntegerField(read_only=True)
    norad_str = serializers.CharField(read_only=True)
    satellite = serializers.IntegerField(source='satellite.id')

