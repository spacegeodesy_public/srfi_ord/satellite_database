from django.urls import include, path
from rest_framework import routers
from . import views

app_name="api"

router = routers.DefaultRouter()
router.register(r'satellite', views.SatelliteViewSet, basename="Satellite")
router.register(r'satellitedata', views.SatelliteDataViewSet, basename="SatelliteData")

urlpatterns = [
    path('api/', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    #path('api/tle-search/', views.TLEViewSet.as_view()),
]