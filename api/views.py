from rest_framework import viewsets, permissions, status
from rest_framework.decorators import api_view, APIView
from rest_framework.response import Response
from website.views import fetch_data, frequency_list_formatter
from datetime import datetime
import pytz
import re

from .serializers import SatelliteDataSerializer, SatelliteSerializer
from database_management.models import Satellite, SatelliteData


class SatelliteViewSet(viewsets.ReadOnlyModelViewSet):
    """ ViewSet for Satellites """

    serializer_class = SatelliteSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self):
        return Satellite.objects.all()


class SatelliteDataViewSet(viewsets.ReadOnlyModelViewSet):
    """ ViewSet for SatelliteData """

    serializer_class = SatelliteDataSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]

    def get_queryset(self, format='search'):
        """ Reads & validates query from URL """

        # query dict
        query = {}

        # time range
        start_datetime = self.request.query_params.get('start-datetime')
        end_datetime = self.request.query_params.get('end-datetime')

        query['start_datetime'] = datetime_formatter(str(start_datetime))
        query['end_datetime'] = datetime_formatter(str(end_datetime))

        if query['start_datetime'] is None:
            query["start_datetime"] = datetime.utcnow()
            query["start_datetime"] = datetime_formatter("20230101T0001")

        if query['end_datetime'] is None:
            query["end_datetime"] = datetime.utcnow()
            query["end_datetime"] = query["end_datetime"].replace(tzinfo=pytz.UTC)

        before = self.request.query_params.get('before')
        after = self.request.query_params.get('after')

        # before
        if before is None or not str(before).isdigit():
            query['before'] = 0
        else:
            query['before'] = int(before)

        # after
        if after is None or not str(after).isdigit():
            query['after'] = 0
        else:
            query['after'] = int(before)

        # norad id
        norad_id = self.request.query_params.get('norad-id')
        if norad_id is None:
            query['norad_id'] = False
        else:
            query['norad_id'] = norad_id

        # object name
        object_name = self.request.query_params.get('object-name')
        if object_name is None:
            query['object_name'] = False
        else:
            query['object_name'] = object_name.replace("+", " ")

        # include satellites without frequency data
        if self.request.query_params.get('without-frequency-data') is None:
            query['without_frequency_data'] = False
        else:
            if self.request.query_params.get('without-frequency-data') == "True":
                query['without_frequency_data'] = True
            else:
                query['without_frequency_data'] = False

        # list of frequencies
        if self.request.query_params.get('frequency-list'):
            query['frequency_list'] = api_frequency_list_formatter(self.request.query_params.get('frequency-list'))
        else:
            query['frequency_list'] = []

        # fetch queryset
        queryset = fetch_data(query)

        return queryset


class TLEViewSet(APIView):
    """
    !!! DEPRECATED !!!
    ViewSet for TLE file/string
    """

    def get(self, request):

        query = {}

        # time range
        start_datetime = self.request.query_params.get('start-datetime')
        end_datetime = self.request.query_params.get('end-datetime')

        query['start_datetime'] = datetime_formatter(str(start_datetime))
        query['end_datetime'] = datetime_formatter(str(end_datetime))

        if query['start_datetime'] is None:
            query["start_datetime"] = datetime.utcnow()
            query["start_datetime"] = datetime_formatter("20230101T0001")

        if query['end_datetime'] is None:
            query["end_datetime"] = datetime.utcnow()
            query["end_datetime"] = query["end_datetime"].replace(tzinfo=pytz.UTC)

        before = self.request.query_params.get('before')
        after = self.request.query_params.get('after')

        # before
        if before is None or not str(before).isdigit():
            query['before'] = 0
        else:
            query['before'] = int(before)

        # after
        if after is None or not str(after).isdigit():
            query['after'] = 0
        else:
            query['after'] = int(before)

        # norad id
        norad_id = self.request.query_params.get('norad-id')
        if norad_id is None:
            query['norad_id'] = False
        else:
            query['norad_id'] = norad_id

        # object name
        object_name = self.request.query_params.get('object-name')
        if object_name is None:
            query['object_name'] = False
        else:
            query['object_name'] = object_name.replace("+", " ")

        #include satellites without frequency data
        if self.request.query_params.get('without-frequency-data') is None:
            query['without_frequency_data'] = False
        else:
            if self.request.query_params.get('without-frequency-data') == "True":
                query['without_frequency_data'] = True
            else:
                query['without_frequency_data'] = False

        # list of frequencies
        if self.request.query_params.get('frequency-list'):
            query['frequency_list'] = api_frequency_list_formatter(self.request.query_params.get('frequency-list'))
        else:
            query['frequency_list'] = []

        # fetch queryset & convert to string
        queryset = fetch_data(query)
        string = "\n".join([obj.norad_str for obj in queryset])

        return Response({'tle-str': string, 'parameters': query})


def datetime_formatter(dt):
    """ takes a datetime string and turns it into a datetime object """

    # matches dates of the format 20231201T1010
    dt_pattern = '\d\d\d\d(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])T(0[0-9]|1[0-9]|2[0-3])([0-5][0-9])'
    matched = re.match(dt_pattern, dt)

    # if the date is valid, read it out
    if bool(matched):
        d, t = dt.split('T')
        new_d = datetime.strptime(d, "%Y%m%d").date()
        new_t = datetime.strptime(t, "%H%M").time()

        # create datetime object and make it aware
        new_dt = datetime.combine(new_d, new_t)
        new_dt = new_dt.replace(tzinfo=pytz.UTC)
        return new_dt
    else:
        return None


def api_frequency_list_formatter(frequency_str):
    """ Takes the frequency list query input and turns it into a validated list of frequencies (float) """

    # convert list
    fl = frequency_str.strip('][').split(',')
    result = []

    for fstr in fl:
        # split for hyphen/minus (almost all unicode hyphens included)
        freq_pair = re.split(r'-|֊|־|᠆|‐|‑|‒|–|—|―|﹘|﹣|－', fstr)
        freq_pair = [entry.strip() for entry in freq_pair if entry.strip() != ""]

        if len(freq_pair) >= 2:
            # find number pairs
            f1 = re.match(r'(\d+(\.\d+)?)', freq_pair[0])
            f2 = re.match(r'(\d+(\.\d+)?)', freq_pair[1])

            if f1 != None and f2 != None:
                new_freq_pair = [abs(float(f1.group(0))), abs(float(f2.group(0)))]
                new_freq_pair = sorted(new_freq_pair)

                result.append(new_freq_pair)
    return result