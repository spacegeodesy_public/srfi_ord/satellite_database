from rest_framework.views import exception_handler

def custom_exception_handler(exc, context):

    """ Raises an exception if an API request takes longer than 30 seconds. """
    if isinstance(exc, TimeoutException):
        return Response(
            {'detail': 'Your request timed out.'},
            status=status.HTTP_408_REQUEST_TIMEOUT
        )
    response = exception_handler(exc, context)
    return response