from django.core.management.base import BaseCommand

from database_management.tle_utils import TLEObject, directory_to_tle_list
from database_management.models import Satellite, SatelliteData

from datetime import datetime, timedelta
import pytz


class Command(BaseCommand):
    help = "Loads all .tle files in a directory into Satellite Database. This command is specific to the development environment and won't work on a different machine."

    """
        def add_arguments(self, parser):
        parser.add_argument("directory", type=str, help="Path to a directory containing .tle files to be loaded into the database")
    """


    def handle(self, *args, **kwargs):

        #dir_path = kwargs['directory']

        dir_path = "C:/Users/Space Geodesy/Documents/Project/Python-Projekte/Pure-Python-Prototype/tle-files"

        tle_lists = directory_to_tle_list(dir_path)

        for filename, tle_strings in tle_lists:

            for tle_str in tle_strings:

                # create TLEObject from the string
                new_tle = TLEObject(tle_str)

                # calculate timestamp
                new_year = datetime.strptime(new_tle.epoch[:5], "%y%j")
                new_seconds = timedelta(seconds=float(new_tle.epoch[5:]) * 24 * 60 * 60)
                new_timestamp = new_year + new_seconds
                new_timestamp = new_timestamp.replace(tzinfo=pytz.UTC)

                # check if Satellite data object exists & retrieve it if it does
                if Satellite.objects.filter(norad_cat_id=new_tle.norad_cat_id).exists():
                    new_satellite = Satellite.objects.get(norad_cat_id=new_tle.norad_cat_id)

                # create Satellite data if it doesn't
                else:
                    new_satellite = Satellite(
                        object_name=new_tle.object_name,
                        norad_cat_id=new_tle.norad_cat_id,
                        int_desg=new_tle.int_desg,
                        ephemeris_type=new_tle.ephemeris_type,
                        tle_filename=filename
                    )
                    new_satellite.save()


                if SatelliteData.objects.filter(satellite=new_satellite).filter(timestamp=new_timestamp).exists():
                    pass

                else:
                    # create SatelliteData object
                    new_satellite_data = SatelliteData(
                        satellite=new_satellite,
                        timestamp=new_timestamp,
                        epoch=new_tle.epoch,
                        mean_motion_dot=new_tle.mean_motion_dot,
                        mean_motion_ddot=new_tle.mean_motion_ddot,
                        bstar=new_tle.bstar,
                        element_set_no=new_tle.element_set_no,
                        inclination=new_tle.inclination,
                        ra_of_asc_node=new_tle.ra_of_asc_node,
                        eccentricity=new_tle.eccentricity,
                        arg_of_pericenter=new_tle.arg_of_pericenter,
                        mean_anomaly=new_tle.mean_anomaly,
                        mean_motion=new_tle.mean_motion,
                        rev_at_epoch=new_tle.rev_at_epoch,
                        norad_str=tle_str

                    )
                    new_satellite_data.save()



