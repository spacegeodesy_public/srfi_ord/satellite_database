from database_management.models import Satellite, SatelliteData
from django.core.management.base import BaseCommand


class Command(BaseCommand):
    help = "Deletes all Satellite and SatelliteData objects. Primarily intended for use in development."

    def handle(self, *args, **options):
        Satellite.objects.all().delete()
        SatelliteData.objects.all().delete()
