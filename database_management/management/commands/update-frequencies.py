from django.core.management.base import BaseCommand
from database_management.models import Satellite, SatelliteData, Frequency, FrequencySource

class Command(BaseCommand):
    help = "Update FrequencySources for all Satellites"

    def handle(self, *args, **options):
        # retrieve all FrequencySources
        sources = FrequencySource.objects.all()

        # reset satellites
        Satellite.objects.all().update(frequency_source=None)

        # update Satellites that match the FrequencySource's regex expression
        for source in sources:
            # make sure an empty regex field won't match
            if source.regex_sat_name.strip() != '':
                sats = Satellite.objects.filter(object_name__iregex=source.regex_sat_name)
                sats.update(frequency_source=source)