from django.core.management.base import BaseCommand

from database_management.tle_utils import TLEObject, scrape_tle_data, epoch_to_timestamp
from database_management.models import Satellite, SatelliteData


class Command(BaseCommand):
    help = "Scrape CelesTrak satellite data and load it into the database."

    def handle(self, *args, **options):
        # scrape data from website
        tle_dict = scrape_tle_data("https://celestrak.org/NORAD/elements/")
        keys = tle_dict.keys()

        for key in keys:
            for tle_str in tle_dict[key]:
                # create TLEObject from the string
                new_tle = TLEObject(tle_str)

                # calculate timestamp
                new_timestamp = epoch_to_timestamp(new_tle.epoch)

                # check if Satellite data object exists & retrieve it if it does
                if Satellite.objects.filter(norad_cat_id=new_tle.norad_cat_id).exists():
                    new_satellite = Satellite.objects.get(norad_cat_id=new_tle.norad_cat_id)

                # create Satellite data if it doesn't
                else:
                    new_satellite = Satellite(
                        object_name=new_tle.object_name,
                        norad_cat_id=new_tle.norad_cat_id,
                        int_desg=new_tle.int_desg,
                        ephemeris_type=new_tle.ephemeris_type,
                        tle_filename=key,
                        frequency_source=new_tle.frequency_source
                    )
                    new_satellite.save()

                # check if SatelliteData is already read in
                if SatelliteData.objects.filter(satellite=new_satellite).filter(timestamp=new_timestamp).exists():
                    pass

                else:
                    # create SatelliteData object
                    new_satellite_data = SatelliteData(
                        satellite=new_satellite,
                        timestamp=new_timestamp,
                        epoch=new_tle.epoch,
                        mean_motion_dot=new_tle.mean_motion_dot,
                        mean_motion_ddot=new_tle.mean_motion_ddot,
                        bstar=new_tle.bstar,
                        element_set_no=new_tle.element_set_no,
                        inclination=new_tle.inclination,
                        ra_of_asc_node=new_tle.ra_of_asc_node,
                        eccentricity=new_tle.eccentricity,
                        arg_of_pericenter=new_tle.arg_of_pericenter,
                        mean_anomaly=new_tle.mean_anomaly,
                        mean_motion=new_tle.mean_motion,
                        rev_at_epoch=new_tle.rev_at_epoch,
                        norad_str=tle_str

                    )
                    new_satellite_data.save()
        
