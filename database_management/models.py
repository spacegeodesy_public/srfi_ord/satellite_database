from django.db import models
from django.db.models import UniqueConstraint
from django.db.models.signals import post_save
from django.dispatch import receiver


class FrequencySource(models.Model):
    """
    Stores the name of a FrequencySource
    """

    class Meta:
        verbose_name = "Frequency Source"
        verbose_name_plural = "Frequency Sources"

    name = models.CharField(verbose_name="Name", max_length=50, unique=True)
    regex_sat_name = models.CharField(verbose_name="Regular Expression for matching Satellite names", max_length=500)

    def __str__(self):
        return self.name


class Frequency(models.Model):
    """
    Stores a frequency range, the time range it was operational for and the FrequencySource it associated with.
    """

    class Meta:
        verbose_name = "Frequency"
        verbose_name_plural = "Frequencies"

    frequency_source = models.ForeignKey(FrequencySource, on_delete=models.CASCADE)
    start_frequency = models.DecimalField(verbose_name="Start of Frequency Range (GHz)", max_digits=25, decimal_places=10)
    end_frequency = models.DecimalField(verbose_name="End of Frequency Range (GHz)", max_digits=25, decimal_places=10)
    start_date = models.DateTimeField(verbose_name="Start Date")
    end_date = models.DateTimeField(verbose_name="End Date", null=True, blank=True)

    def __str__(self):
        if self.end_frequency == None:
            end = "now"
        else:
            end = str(self.end_frequency).rstrip('0')
        return self.frequency_source.name + " (" + str(self.start_frequency).rstrip('0') + " - " + end + ")"


class Satellite(models.Model):
    """
    Stores non-historical data for a Satellite, mostly for identification.
    """

    class Meta:
        verbose_name = "Satellite"
        verbose_name_plural = "Satellites"
        indexes = [models.Index(fields=["norad_cat_id"])]

    # database rows
    object_name = models.CharField(max_length=250, verbose_name="Satellite Name")
    norad_cat_id = models.IntegerField(verbose_name="NORAD number", unique=True)
    int_desg = models.CharField(max_length=50, verbose_name="International Designator")
    ephemeris_type = models.IntegerField(verbose_name="Ephemeris Type")
    frequency_source = models.ForeignKey(FrequencySource, on_delete=models.CASCADE, blank=True, null=True)
    tle_filename = models.CharField(max_length=50, verbose_name="Name of .tle file")

    def __str__(self):
        return self.object_name + " (" + str(self.norad_cat_id) + ")"


class SatelliteData(models.Model):
    """
    Stores historical data for a Satellite
    """

    class Meta:
        verbose_name = "Satellite Data Entry"
        verbose_name_plural = "Satellite Data Entries"
        # satellite & timestamp combination must be unique (prevents duplicates and conflicting data)
        constraints = [UniqueConstraint(fields=["satellite", "timestamp"], name="satellite_timestamp_unique_together")]

    # database rows
    satellite = models.ForeignKey(Satellite, on_delete=models.CASCADE)
    timestamp = models.DateTimeField()
    epoch = models.CharField(verbose_name="Epoch", max_length=50)
    mean_motion_dot = models.CharField(verbose_name="First Derivative of Mean Motion", max_length=50)
    mean_motion_ddot = models.CharField(verbose_name="Second Derivative of Mean Motion", max_length=50)
    bstar = models.CharField(verbose_name="BSTAR Drag Term", max_length=50)
    element_set_no = models.IntegerField(verbose_name="Element Number")
    inclination = models.DecimalField(verbose_name="Inclination", max_digits=7, decimal_places=4)
    ra_of_asc_node = models.DecimalField(verbose_name="Right Ascension of the Ascending Node", max_digits=8, decimal_places=4)
    eccentricity = models.DecimalField(verbose_name="Eccentricity", max_digits=8, decimal_places=7)
    arg_of_pericenter = models.DecimalField(verbose_name="Argument of Perigee", max_digits=7, decimal_places=4)
    mean_anomaly = models.DecimalField(verbose_name="Mean Anomaly", max_digits=7, decimal_places=4)
    mean_motion = models.DecimalField(verbose_name="Mean Motion", max_digits=14, decimal_places=10)
    rev_at_epoch = models.IntegerField(verbose_name="Revolution number at epoch")
    norad_str = models.CharField(verbose_name="Unaltered NORAD string", max_length=500)

    def __str__(self):
        return "%s ; %s" % (self.satellite, str(self.timestamp))


""" Signals """

@receiver(post_save, sender=FrequencySource)
def update_satellite_frequency_source(sender, instance, **kwargs):
    # delete previously assigned satellites
    prev_sats = Satellite.objects.filter(frequency_source=instance)
    prev_sats.update(frequency_source=None)
    # fetch matching sats & update
    new_sats = Satellite.objects.filter(object_name__iregex=instance.regex_sat_name)
    new_sats.update(frequency_source=instance)
