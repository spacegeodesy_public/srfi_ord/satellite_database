from django.contrib import admin
from .models import Satellite, SatelliteData, FrequencySource, Frequency


# customize admin page
class SatelliteAdmin(admin.ModelAdmin):
    # columns displayed on admin page
    list_display = ("object_name", "norad_cat_id", "frequency_source")
    search_fields = ["object_name", "norad_cat_id", "frequency_source__name"]

class SatelliteDataAdmin(admin.ModelAdmin):
    list_display = ("satellite", "timestamp")
    search_fields = ["satellite__object_name"]


class FrequencyAdmin(admin.ModelAdmin):
    list_display = ("frequency_source", "start_frequency", "end_frequency")
    search_fields = ['frequency_source__name']


# register models
admin.site.register(Satellite, SatelliteAdmin)
admin.site.register(SatelliteData, SatelliteDataAdmin)
admin.site.register(FrequencySource)
admin.site.register(Frequency, FrequencyAdmin)