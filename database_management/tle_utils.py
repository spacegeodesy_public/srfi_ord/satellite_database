import re
import requests
from bs4 import BeautifulSoup
from decimal import *
import os
from pathlib import Path
from datetime import datetime, timedelta
import pytz
from database_management.models import FrequencySource


class TLEObject:
    """
    Contains all the information read from 3 lines of a .tle file.
    """

    def __init__(self, tle_string):
        """
        Initializes the TLEObject and reads out all the information from tle_string.
        :param tle_string: string containing the three lines of a TLE object
        """
        self._line1 = list(tle_string.split("\n")[1])
        self._line2 = list(tle_string.split("\n")[2])

        self._unaltered_string = tle_string

        # line 0
        self.object_name = tle_string.split("\n")[0].strip()

        # line 1
        self.norad_cat_id = self._clean_join(self._line1[2:7])
        self.classification_type = self._line1[7]
        self.int_desg = self._clean_join(self._line1[9:17])
        self.epoch = self._clean_join(self._line1[18:32])
        self.mean_motion_dot = self._clean_join(self._line1[33:43])
        self.mean_motion_ddot = self._clean_join(self._line1[44:52])
        self.bstar = self._clean_join(self._line1[53:61])
        self.ephemeris_type = self._line1[62]
        self.element_set_no = self._clean_join(self._line1[64:68])
        self._checksum1 = self._line1[68]

        # line 2
        self._norad_cat_id_line2 = self._clean_join(self._line2[2:7])
        self.inclination = self._clean_join(self._line2[8:16])
        self.ra_of_asc_node = self._clean_join(self._line2[17:25])
        self.eccentricity = self._clean_join(self._line2[26:33])
        self.arg_of_pericenter = self._clean_join(self._line2[34:42])
        self.mean_anomaly = self._clean_join(self._line2[43:51])
        self.mean_motion = self._clean_join(self._line2[52:63])
        self.rev_at_epoch = self._clean_join(self._line2[63:68])
        self._checksum2 = self._clean_join(self._line2[68])

        # frequency source
        self.frequency_source = self._find_frequency()

        # verify that data was read in correctly
        self._verify_data()

        # convert entries from strings to their final data type
        self._update_data_types()

    def __repr__(self):
        return "TLEObject (" + self.object_name + ")"

    def __str__(self):
        return self.object_name

    def _clean_join(self, lst):
        """
        Takes a list of character and joins and cleans them.
        :param lst: list of characters
        :return: joined and cleaned string
        """
        return "".join(lst).strip()

    def _verify_data(self):
        """
        Verifies if the data was read in correctly.
        Checks if the checksums read from the data match the checksum calculated from the data entries
        and compares the satellite names from line 1 and line 2.
        """
        # data read from line 1
        l1 = [
            self.norad_cat_id,
            self.classification_type,
            self.int_desg,
            self.epoch,
            self.mean_motion_dot,
            self.mean_motion_ddot,
            self.bstar,
            self.ephemeris_type,
            self.element_set_no
        ]

        # data read from line 2
        l2 = [
            self._norad_cat_id_line2,
            self.inclination,
            self.ra_of_asc_node,
            self.eccentricity,
            self.arg_of_pericenter,
            self.mean_anomaly,
            self.mean_motion,
            self.rev_at_epoch
        ]

        # raise Exception if calculated checksum doesn't match read-out checksum
        if not self._checksum(l1, 1, self._checksum1) or not self._checksum(l2, 2, self._checksum2):
            raise Exception("TLE data couldn't be verified. Checksums don't match.")

        # raise Exception if satellite number from lines 1 and 2 don't match
        if self.norad_cat_id != self._norad_cat_id_line2:
            raise Exception("TLE data couldn't be verified. Satellite numbers don't match.")

    def _find_frequency(self):
        """
        Checks if any of the FrequencySources match
        """
        sources = FrequencySource.objects.all()
        for source in sources:
            # make sure it doesn't match an empty field
            if source.regex_sat_name.strip() != '':
                if re.match(source.regex_sat_name, self.object_name):
                    return source
        return None



    def _checksum(self, list_of_entries, line_nr, check):
        """
        Calculates the TLE Checksum of a list of data entries and compares it to the Checksum read out of the data.
        :param list_of_entries: list of database entries taken from a line of .tle data
        :param line_nr: 1 or 2, depending on which line is being verified
        :param check: checksum taken from the .tle data
        """
        num_sum = line_nr
        for entry in list_of_entries:
            for char in list(entry):
                # add 1 for every minus
                if char == "-":
                    num_sum += 1
                # sum up numbers
                elif char.isnumeric():
                    num_sum += int(char)

        # modulo 10
        check_sum = num_sum % 10

        return int(check_sum) == int(check)

    def _update_data_types(self):
        """
        Updates all the string values into their proper data types. To be run after data has been verified.
        """
        self.ephemeris_type = int(self.ephemeris_type)
        self.element_set_no = int(self.element_set_no)
        self.inclination = Decimal(self.inclination)
        self.ra_of_asc_node = Decimal(self.ra_of_asc_node)
        self.eccentricity = Decimal("0." + self.eccentricity)
        self.mean_anomaly = Decimal(self.mean_anomaly)
        self.mean_motion = Decimal(self.mean_motion)
        self.rev_at_epoch = int(self.rev_at_epoch)


def find_tle_strings(tle_str):
    """
    Takes a read-out .tle file and finds the complete .tle objects in it
    """

    lines = tle_str.splitlines()
    tle_strings = []

    # filter for three-line combinations ["...", "1 ...", "2 ..."]
    for i, line in enumerate(lines):
        try:
            if line.startswith("1 ") and lines[i + 1].startswith("2 "):
                tle_entry = "\n".join([lines[i - 1], line, lines[i + 1]])
                tle_strings.append(tle_entry)
        except IndexError:
            pass

    return tle_strings


def scrape_tle_data(celestrak_url):
    """
    Takes the link to the CelesTrak website and retrieves all .tle files.
    :param celestrak_url:
    :return: tle_dictionary: a dictionary of the format: { "Filename1" : [ "tle_string1", "tle_string2"], ...}
    """
    # dictionary containing all the TLE-Data
    tle_dictionary = {}

    # collect and parse the Celestrak HTML
    response = requests.get(celestrak_url)
    soup = BeautifulSoup(response.content, "html.parser")
    html_element_list = soup.find_all("a", title="TLE Data")

    # iterate over .tle files and retrieve their content
    for element in html_element_list:
        file_name = element.text
        file_url = element["href"]

        response = requests.get(celestrak_url + file_url)

        # find all TLE-Objects in the file
        tle_strings = find_tle_strings(response.content.decode("utf-8").replace("\r", ""))

        # save data to dictionary
        tle_dictionary[file_name] = tle_strings

    return tle_dictionary


def epoch_to_timestamp(epoch):
    """
    Takes Julian Date Epoch and returns it as a datetime object.
    """
    new_year = datetime.strptime(epoch[:5], "%y%j")
    new_seconds = timedelta(seconds=float(epoch[5:]) * 24 * 60 * 60)
    new_timestamp = new_year + new_seconds
    new_timestamp = new_timestamp.replace(tzinfo=pytz.UTC)
    return new_timestamp


