from django.urls import path
from . import views
from django.conf import settings
from django.conf.urls.static import static


urlpatterns = [
    path('', views.homepage, name='homepage'),
    path('search/', views.search_results, name='search_results'),
    path('download-search/', views.search_results_download, name='search_results_download'),
    path('about/', views.about, name='about'),
    path('api-documentation/', views.api_documentation, name='api_documentation')
]



urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)