from django.shortcuts import render
from django.http import HttpResponse
from database_management.models import Satellite, SatelliteData, Frequency, FrequencySource
from django.db.models import Q, Max, Min, OuterRef, Subquery, Sum, Prefetch, F
from datetime import datetime, time, timedelta
import re
import pytz




def homepage(request):
    """ Homepage """

    now = datetime.utcnow()
    now = now.replace(tzinfo=pytz.UTC)

    # pass FrequencySources as context for drop-down menu
    context = {
        "total_nr_tles": format(SatelliteData.objects.all().count(), ",").replace(",", "'"),
        "total_nr_sats": format(Satellite.objects.all().count(), ",").replace(",", "'"),
        "tles_last_7": format(SatelliteData.objects.filter(timestamp__gte=now-timedelta(days=7)).count(), ",").replace(",", "'"),
        "latest_tle": SatelliteData.objects.latest("timestamp").timestamp.strftime('%Y-%m-%d %H:%M')
    }
    return render(request, "homepage.html", context=context)



def search_results(request):
    """ Search Results Page """
    validated_query, results, query_str = {}, {}, {}
    if request.method == 'POST':
        # populate form with data from request
        query = request.POST
        # validate and clean query
        validated_query = clean_query(query)
        # fetch queryset
        results = fetch_data(validated_query)
        # count results
        results_length = results.count()
        # slice results
        results = results[:500]
        # transform results into list
        results = results.values("satellite__object_name", "norad_str")
        # cache query
        request.session['query'] = query
        # query string
        query_str = {
            "start": validated_query['start_datetime'].strftime('%Y-%m-%d %H:%M'),
            "end": validated_query['end_datetime'].strftime('%Y-%m-%d %H:%M'),
            "length": results_length,
            "api_call": query_to_api(validated_query)
        }

    context = {"query": validated_query, "query_str": query_str, "results": results}

    return render(request, "search_results.html", context=context)


def api_documentation(request):
    """ API Doucmentation Page """
    return render(request, "api_documentation.html", context={})


def search_results_download(request):
    """ Download Search Results Button """

    # get cached query & results
    query = request.session['query']
    validated_query = clean_query(query)
    results = fetch_data(validated_query)

    # write response to txt file
    response = HttpResponse(content_type="text/plain")
    filename = "tle.txt"
    response['Content-Disposition'] = 'attachment; filename=' + filename

    # use paginator
    """
    paginator = Paginator(results, 100)
    for page_number in paginator.page_range:
        page = paginator.page(page_number)
        norad_strs = page.object_list.values_list('norad_str', flat=True)
    """
    norad_strs = results.values_list('norad_str', flat=True)
    response.writelines(line + '\n' for line in list(norad_strs))
    return response


def about(request):
    """ About Page """
    context = {}
    return render(request, "about.html", context=context)


def clean_query(query):
    """ Takes raw query and cleans & validates it """
    cleaned = {"message": ""}

    # check if start date was entered
    if query.get("start-date"):
        start_date = query.get("start-date")
        
        # check if start time was entered
        if query.get("start-time") != "":
            start_time = query.get("start-time")
        # if not, assume midnight
        else:
            start_time = time(0, 0).strftime("%H:%M")

        # set start timestamp
        cleaned["start_datetime"] = datetime_formatter(start_date, start_time)
    else:
        cleaned["start_datetime"] = datetime_formatter("2023-01-01", "00:01")
        cleaned["message"] = cleaned["message"] + "You didn't enter a start date, so we set it to include all available data. \n"

    if query.get("end-date"):
        end_date = query.get("end-date")

        # check if time was entered
        if query.get("end-time"):
            end_time = query.get("end-time")
        # if not, assume end of day
        else:
            end_time = time(23, 59).strftime("%H:%M")

        # set end timestamp
        cleaned["end_datetime"] = datetime_formatter(end_date, end_time)
    # if no datetime was provded, set to now
    else:
        cleaned["end_datetime"] = datetime.utcnow()
        cleaned["end_datetime"] = cleaned["end_datetime"].replace(tzinfo=pytz.UTC)
        cleaned["message"] = cleaned["message"] + "You didn't enter an end date, so we set it to now. \n"

    # check if end date is before start date
    if cleaned["start_datetime"] != None and cleaned["end_datetime"] != None:
        if cleaned["start_datetime"] > cleaned["end_datetime"]:
            cleaned["message"] = cleaned["message"] + "The entered end date is before the start date, so we swapped them for you. If this isn't what you wanted, please re-enter the search query. \n"
            start_temp = cleaned["start_datetime"]
            cleaned["start_datetime"] = cleaned["end_datetime"]
            cleaned["end_datetime"] = start_temp

    before = query.get('before')
    after = query.get('after')

    # before
    if before is None or not str(before).isdigit():
        cleaned['before'] = 0
    else:
        cleaned['before'] = int(before)

    # after
    if after is None or not str(after).isdigit():
        cleaned['after'] = 0
    else:
        cleaned['after'] = int(after)

    # satellite name
    cleaned['object_name'] = query.get('satellite-name').strip()

    # norad id
    if query['norad-nr'].strip().isdigit():
        cleaned['norad_id'] = int(query.get('norad-nr').strip())
    else:
        cleaned['norad_id'] = False

    # frequencies
    if query['frequency-list'].strip() != '':
        cleaned['frequency_list'] = frequency_list_formatter(query['frequency-list'].strip())
    else:
        cleaned['frequency_list'] = []

    # if satellites without frequency data are included
    if query.get('without-frequency-data'):
        if query.get('without-frequency-data') == 'on':
            cleaned['without_frequency_data'] = True
        else:
            cleaned['without_frequency_data'] = False
    else:
        cleaned['without_frequency_data'] = False
    return cleaned


def frequency_list_formatter(frequency_str):
    """ Takes the frequency list query input and turns it into a validated list of frequencies (float) """

    # split lines
    fl = frequency_str.strip().splitlines()
    result = []

    for fstr in fl:
        # split for hyphen/minus (almost all unicode hyphens included)
        freq_pair = re.split(r'-|֊|־|᠆|‐|‑|‒|–|—|―|﹘|﹣|－', fstr)
        freq_pair = [entry.strip() for entry in freq_pair if entry.strip() != ""]


        if len(freq_pair) >= 2:
            # find number pairs
            f1 = re.match(r'(\d+(\.\d+)?)', freq_pair[0])
            f2 = re.match(r'(\d+(\.\d+)?)', freq_pair[1])

            if f1 != None and f2 != None:

                new_freq_pair = [abs(float(f1.group(0))), abs(float(f2.group(0)))]
                new_freq_pair = sorted(new_freq_pair)

                result.append(new_freq_pair)

    return result


def datetime_formatter(d, t):
    """ takes a date and time and turns it into a datetime object """
    new_d = datetime.strptime(d, "%Y-%m-%d").date()
    new_t = datetime.strptime(t, "%H:%M").time()

    # create datetime object and make it aware
    new_dt = datetime.combine(new_d, new_t)
    new_dt = new_dt.replace(tzinfo=pytz.UTC)
    return new_dt


def fetch_data(query):
    """
    Takes a validated query and returns SatelliteData queryset
    """

    filters = Q(timestamp__range=[query['start_datetime'] - timedelta(days=query['before']),
                                                            query['end_datetime'] + timedelta(days=query['after'])])
    # object name
    if query['object_name']:
        filters &= Q(satellite__object_name__icontains=query['object_name'])

    # norad id search
    if query['norad_id'] != False:
        filters &= Q(satellite__norad_cat_id=query['norad_id'])

    # if no valid frequency pair is given and 'without frequency data' is false, return all entries with frequency data
    if query['frequency_list'] == [] and query['without_frequency_data'] == False:
        filters &= Q(satellite__frequency_source__isnull=False)

    sat_qs = SatelliteData.objects.prefetch_related("satellite").filter(filters)

    # if there are valid frequencies
    if query['frequency_list'] != []:

        # filter frequencies who were active in the given date range

        freqs = Frequency.objects.all()

        # limit frequencies to those that have an overlapping start & end date
        temp_startdate = query['start_datetime']
        temp_enddate = query['end_datetime']

        end_qry = Q(end_date__gte=temp_enddate) | Q(end_date__isnull=True)

        # start/end date are smaller/bigger
        freq1 = freqs.filter(start_date__lte=temp_startdate).filter(end_qry)

        # start or end date are in range of freq pair
        freq2 = freqs.filter(start_date__range=[temp_startdate, temp_enddate])
        freq3 = freqs.filter(end_date__range=[temp_startdate, temp_enddate])

        # add to matching frequencies
        freqs = freq1 | freq2 | freq3

        # filter frequencies who match the frequencies

        result_freqs = Frequency.objects.none()

        # find all frequencies that match each frequency pair
        for freq_pair in query['frequency_list']:

            # start/end date are smaller/bigger
            new_freq1 = freqs.filter(Q(start_frequency__lte=freq_pair[0]) & Q(end_frequency__gte=freq_pair[1]))

            # start or end date are in range of freq pair
            new_freq2 = freqs.filter(start_frequency__range=freq_pair)
            new_freq3 = freqs.filter(end_frequency__range=freq_pair)

            # add to matching frequencies
            result_freqs = result_freqs | new_freq1 | new_freq2 | new_freq3

        # filters for satellites with a frequency source, whose frequencies are in freqs
        sat_qs_with_f = sat_qs.filter(satellite__frequency_source__frequency__in=result_freqs)

        # include data without frequency source
        if query['without_frequency_data'] == True:
            sat_qs_without_f = sat_qs.filter(satellite__frequency_source__isnull=True)
            sat_qs = sat_qs_with_f | sat_qs_without_f
        else:
            sat_qs = sat_qs_with_f

    # if no valid frequency pair is given and without-frequency-data is true, just continue

    # retrieve timestamp data
    during = sat_qs.filter(
        timestamp__range=[query['start_datetime'], query['end_datetime']]
    )

    results = during

    if query['before'] > 0:
        before = sat_qs.filter(
            timestamp=Subquery(
                sat_qs.filter(timestamp__lte=query['start_datetime'])
                    .filter(satellite=OuterRef('satellite'))
                    .annotate(before_date=Max(F('timestamp')))
                    .values('before_date')[:1]
                )
        )
        results = results | before

    if query['after'] < 0:
        after = sat_qs.filter(
            timestamp=Subquery(
                sat_qs.filter(timestamp__gte=query['end_datetime'])
                    .filter(satellite=OuterRef('satellite'))
                    .annotate(after_date=Min(F('timestamp')))
                    .values('after_date')[:1]
            )
        )
        results = results | after

    results = results.order_by('satellite', 'timestamp')

    #results = SatelliteData.objects.prefetch_related("satellite").filter(filters)

    return results



def tle_fetcher(query):
    """ Retrieves the TLE strings when downloading the results as a .txt file """

    results = fetch_data(query)
    results = results.values_list('norad_str', flat=True)
    norad_strs = list(results)
    return norad_strs


def query_to_api(query):
    url = "https://satdb.ethz.ch/api/satellitedata/?"

    # start and end date
    url += "start-datetime=" + query['start_datetime'].strftime("%Y%m%dT%H%M")
    url += "&end-datetime=" + query['end_datetime'].strftime("%Y%m%dT%H%M")

    # before & after
    if query['before']:
        url += "&before=" + str(query['before'])
    if query['after']:
        url += "&after=" + str(query['after'])

    # name & norad
    if query['object_name']:
        url += "&object-name=" + query['object_name'].strip().replace(" ", "+")
    if query['norad_id']:
        url += "&norad-id=" + str(query['norad_id'])

    # frequencyies
    url += "&without-frequency-data=" + str(query['without_frequency_data'])

    if query['frequency_list'] != []:
        url += "&frequency-list=" + "["
        for freq_pair in query['frequency_list']:
            url += str(freq_pair).strip("[]").replace(" ", "").replace(",", "-") + ","
        url = url.rstrip(",")
        url += "]"

    return url


