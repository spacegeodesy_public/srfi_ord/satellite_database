<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="author" content="Space Geodesy Group, ETH Zurich, Developer: Luisa Wunderlin">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Satellite Database</title>

    {% load static %}
    <link rel="stylesheet" type="text/css" href="{% static 'css/fonts.css' %}">
    <link rel="stylesheet" type="text/css" href="{% static 'css/styles.css' %}">
    <script src="{% static 'js/homepage.js' %}"></script>
    <link rel="shortcut icon" type="image/x-icon" href="{% static 'images/satellite_favicon.png' %}">
</head>


<body>

<!-- HEADER -->
{% include "header.html" %}


<!-- BODY -->
<div class="body-wrapper">

    <h1>API</h1>

    <p>
        The API offers the same functionality as the TLE search on the <a href="/">homepage</a>.
    </p>


    <h2>Constructing a Query</h2>

    <p>
        The API works through GET requests, which means all the query parameters are passed through the URL.
        To make a request, you can either construct the URL manually
        or use CURL or the Python requests library to do it for you.
        We will give examples for all three.
    </p>
    <p>
        The query parameters are identical to the ones on the TLE search form on the homepage
        and you can read more about how exactly they work there.
        Searching using the homepage will also return the URL for an equivalent (already validated) API request,
        so you can also use the homepage for constructing your queries.
    </p>

    <p>
        The Satellite Data API returns a JSON object with three key/value pairs:
        <ul>
        <li>"results" contains a list of dictionaries, with each dictionary consisting of a TLE "id", "norad_str" and "satellite",
        with "norad_str" being the Two-Line Element in string format.</li>
        <li>"next" is the URL for the next API request. "results" only contains 500 TLEs at a time,
        so for big requests, you will have to iterate through the "next" URLs until "next" is null.</li>
        <li>"previous" is the URL for the previous API request, similarly to "next".
            Allows you to go back in the result pages.</li>
        </ul>
    </p>

    <h3>Parameters</h3>

    <table>
        <tr>
            <th>name</th>
            <th>format</th>
            <th>description</th>
        </tr>
        <tr>
            <td>start-datetime</td>
            <td>YYYYMMDDThhmm</td>
            <td>Date and time. Required.</td>
        </tr>
        <tr>
            <td>end-datetime</td>
            <td>YYYYMMDDThhmm</td>
            <td>Date and time. Required.</td>
        </tr>
        <tr>
            <td>before</td>
            <td>int</td>
            <td rowspan="2">Nr of days. Optional. Set to 0 by default. <br>
                If set to more than 0, returns an additional TLE outside the start and end range.
                For e.g. after, it will select the next entry after the end date, but won't search further than the nr of days given.
                This applies for every satellite retrieved by the search. </td>
        </tr>
        <tr>
            <td>after</td>
            <td>int</td>
        </tr>
        <tr>
            <td>object-name</td>
            <td>str</td>
            <td>Optional. Case-insensitive, returns all that contain search term. Instead of whitespaces use "+". </td>
        </tr>
        <tr>
            <td>norad-id</td>
            <td>int</td>
            <td>Optional. Must match exactly.</td>
        </tr>

	<tr>
		<td>frequency-list</td>
		<td>str of a list</td>
		<td>Optional. List of Frequency Ranges, e.g. [10.0-12.2,13.5-14.0].
	</tr>

        <tr>
            <td>without-frequency-data</td>
            <td>boolean</td>
            <td>Required. True or False.</td>
        </tr>
    </table>

    <h3>Example Request in the URL</h3>

    <p>
        The base URL for a TLE search API request is <code>https://satdb.ethz.ch/api/satellitedata/</code>.
        All search terms are then added after a question mark and concatenated with <code>&</code>,
        so we end up with a URL like this: <code>base-url/?param1=value&#38;param2=value</code>
    </p>
    <p>
        For our example request, we want to retrieve all TLE entries for the International Space Station
        on 09.04.2023. We also want one additional TLE before and after.
    </p>
    <p>
        The ISS has the NORAD-ID 25544, which we specify as the parameter <code>norad-id=25544</code>.
        We set the start date to 09.04.2023 at midnight and the end date the next day also at midnight.
        This translates to the parameters <code>start-datetime=20230409T0000</code> and
        <code>end-datetime=20230410T0000</code>.
        We include 3 days before (<code>before=3</code>) and after (<code>after=3</code>) in order to get the two additional TLEs.
        Lastly, we set <code>without-frequency-data=True</code>.
    </p>
    <p>
        Adding all these parameters to the base URL, we get:
    </p>
    <pre>

        https://satdb.ethz.ch/api/satellitedata/?norad-id=25544&start-datetime=20230409T0000&end-datetime=20230410T0000&before=3&after=3&without-frequency-data=True

    </pre>
    <p>
        To look at the results for this query, you can go <a href="https://satdb.ethz.ch/api/satellitedata/?norad-id=25544&start-datetime=20230409T0000&end-datetime=20230410T0000&before=3&after=3">here</a>.
    </p>

    <h3>Example Request with Curl</h3>

    <p>
        You can use curl to make simple and easy API requests.
    </p>
    <p>
        We can make a request from the command line with <code style="font-size:1rem">curl -G url -d parameter=value</code>.
        This command takes the URL and appends the parameters (indicated by -d) before making a GET request.
        Following this syntax, our previous example request looks like this:
    </p>
    <pre>

        $ curl -G https://satdb.ethz.ch/api/satellitedata/ \
            -d start-datetime=20230409T0000 \
            -d end-datetime=20230410T0000 \
            -d before=3 \
            -d after=3 \
            -d norad-id=25544 \
            -d without-frequency-data=True
    </pre>

    <p>
        If you are trying to fetch more than 500 TLEs, you will also have to request the following
        pages using the "next" URLs.
    </p>


    <h3>Example Request in Python</h3>

    <p>
        Here's an example on how to use the API in Python.
        The search parameters are the same as in the example above, and we are using the requests library to make our request.
        Additionally, we will loop over all (potential) pages and combine them into one string, 'tle_str', which has
        the same format as a TLE file.
    </p>

    <pre>

        import requests

        url = <span style="color:#005910">'https://satdb.ethz.ch/api/satellitedata'</span>
        params = {
                <span style="color:#005910">'start-datetime'</span>: <span style="color:#005910">'20230409T0000'</span>,
                <span style="color:#005910">'end-datetime'</span>: <span style="color:#005910">'20230410T0000'</span>,
                <span style="color:#005910">'before'</span>: <span style="color:DarkBlue">3</span>,
                <span style="color:#005910">'after'</span>: <span style="color:DarkBlue">3</span>,
                <span style="color:#005910">'norad-id'</span>: <span style="color:DarkBlue">25544</span>,
                <span style="color:#005910">'without-frequency-data'</span>: <span style="color:#cc6a25">True</span>,
		<span style="color:grey"># 'frequency-list': '[10.7-12.7,13.85-14.5]'</span>,
		<span style="color:grey"># 'object-name': 'ISS'</span>
                }

        <span style="color:grey"># initial request</span>
        response = requests.get(url, params)
        next, results = response.json()[<span style="color:#005910">'next'</span>], response.json()[<span style="color:#005910">'results'</span>]
        tle_str = <span style="color:#005910">'\n'</span>.join(item[<span style="color:#005910">'norad_str'</span>] for item in results)

        <span style="color:grey"># make next request</span>
        while next != None:
            response = requests.get(next)
            results = response.json()[<span style="color:#005910">'results'</span>]
            tle_str += <span style="color:#005910"> '\n'</span> +  <span style="color:#005910">'\n'</span>.join(item[<span style="color:#005910">'norad_str'</span>] for item in results)
            next = response.json()[<span style="color:#005910">'next'</span>]

    </pre>


    <h2>Retrieving Satellites</h2>

    <p>
        While you can’t search satellites directly, if you want to know more about a satellite a
        TLE entry is associated with, you can query it using its database ID.
        Note that this is NOT the same as the NORAD ID.
    </p>
    <p>
        You can find an associated satellite's ID when querying TLEs with '/api/satellitedata' under 'satellite'.
        To look up the satellite, simply append the ID to the url 'api/satellite/', so e.g. <a href="/api/satellite/26173">api/satellite/26173</a> for the ISS.
    </p>




</div>



<!-- FOOTER -->

{% include 'footer.html' %}


</body>
</html>
