from database_management.models import Satellite, SatelliteData, Frequency, FrequencySource







""""
matches = [r'STARLINK']

# iterate over FrequencySources
# for source in sources:
for match in matches:
    print(Satellite.objects.filter(object_name__iregex=match))



"""





"""
# make request

# option 1: construct request in url
url1 = "http://127.0.0.1:8000/api/tle-search/?object-name=starlink"
response1 = requests.get(url1)

# option 2: pass queries as params
url2 = "http://127.0.0.1:8000/api/tle-search"

params = {"start-datetime": "20230209T0000",
          "end-datetime": "20230210T0000",
          "before": 3,
          "norad-id": 25544
          }

response = requests.get(url2, params)

print("\n")
print("Status Code:", response.status_code)

# check if request was successful
if response.status_code == 200:
    # load json data
    json_dict = response.json()

    # print parameters
    for param in json_dict['parameters']:
        print(param, ":", json_dict['parameters'][param])

    # print first 4 TLEs
    lines = json_dict['tle-str'].split('\n')
    print(len(lines)/3)

    for line in lines[:12]:
        print(line)
    print("...")


print("---")

url3 = "http://127.0.0.1:8000/api/satellitedata"
response3 = requests.get(url3, params)

print("Status Code:", response.status_code)

# check if request was successful
if response.status_code == 200:
    # load json data
    json_dict = response3.json()
    print(json_dict)
    
    
"""

import requests

url = 'https://satdb.ethz.ch/api/tle-search'
params = {
    'start-datetime': '20230409T0000',
    'end-datetime': '20230410T0000',
    'before': 3,
    'after': 3,
    'norad-id': 25544
}

response = requests.get(url, params)
tle_str = response.json()['tle-str']
print(tle_str)